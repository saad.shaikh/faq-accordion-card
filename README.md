# Title: FAQ Accordion Card

Tech stack: Vue.js and Vite

Deployed project: https://saad-shaikh-faq-accordion-card.netlify.app/

## Main tasks:
- Created an accordion without JavaScript
- Created a background image with multiple images that have different values

## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
